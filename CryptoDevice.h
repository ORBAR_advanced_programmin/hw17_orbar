#pragma once

#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include "osrng.h"
#include "modes.h"
#include <md5.h>
#include <hex.h>
class CryptoDevice
{

public:
	std::string encryptAES(std::string);
	std::string decryptAES(std::string);
	std::string encryptMD5(std::string text);

private:
	CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

};
